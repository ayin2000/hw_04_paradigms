package com.example.inclassexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Network;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;

import com.example.inclassexample.utilities.NetworkUtils;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText mSearchTermEditText;
    private Button mSearchButton;
    private Button mResetButton;
    private TextView mSearchResultsDisplay;
    private static String response;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchTermEditText = (EditText)findViewById(R.id.et_search_box);
        mSearchButton = (Button) findViewById(R.id.button_search);
        mResetButton = (Button) findViewById(R.id.button_reset);
        mSearchResultsDisplay = (TextView) findViewById(R.id.tv_display_text);

        makeNetworkSearchQuery();

        mSearchButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        String searchText = mSearchTermEditText.getText().toString();

                        ArrayList<String> pokemonList = NetworkUtils.parsePokemon(response);

                        for(String name : pokemonList){
                            if(name.toLowerCase().equals(searchText.toLowerCase())){
                                mSearchResultsDisplay.setText(name+" Type: ");
                                ArrayList<String> types = NetworkUtils.typeReturn(response,name);
                                for (String pokemon : types){
                                    String formatted = pokemon
                                            .replace("[", "")
                                            .replace("]", "")
                                            .replace("\"", "")
                                            .trim();
                                    mSearchResultsDisplay.append(formatted);
                                }

                                break;
                            }else{
                                mSearchResultsDisplay.setText("No results match.");
                            }
                        }
                    } // end of onClick method

                } // end of View.OnClickListener
        ); // end of setOnClickListener


        mResetButton.setOnClickListener(
                new View.OnClickListener(){ // a unnamed object
                    //inner method def
                    public void onClick(View v){
                        // reset the text
                        //mSearchResultsDisplay.setText(defaultDisplayText);
                        makeNetworkSearchQuery();

                    } // end of onClick method

                } // end of View.OnClickListener
        ); // end of setOnClickListener


    } // end of onCreate

    /* Networking related code begins */
    public void makeNetworkSearchQuery(){
        // get the search string
        String searchTerm = mSearchTermEditText.getText().toString();
        //reset the search results
        mSearchResultsDisplay.setText("Pokemon Database : \n");
        // make the search - network
        new FetchNetworkData().execute(searchTerm);

    }


    // inner class definiton
    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params){ // input parameter is string array
            // get serach term
            if(params.length == 0) return null;
            String searchTerm = params[0];
            //get URL from networkUtils
            URL countryUrl = NetworkUtils.buildCountriesUrl();
            // get response from URL NetworkUtils
            String responseData = null;
            try{
                responseData = NetworkUtils.getResponseFromUrl(countryUrl);
            } catch(IOException e){
                e.printStackTrace();
                Log.d("debug", "getting response from url failed");
            }

            // return response to onPostExecute
            response = responseData;
            return responseData;
        }

        @Override
        protected void onPostExecute(String responseData){
            Log.d("debug", "response recived from URL");
            ArrayList<String> pokemonList = NetworkUtils.parsePokemon(responseData);
            for(String pokemonName : pokemonList){
                mSearchResultsDisplay.append("\n\n" + pokemonName);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // inflates the menu. make sure it is attached to mainactivity page
        Log.d("debug","entered onCreateOptions");
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("debug","entered onOptionsItemSelected");
        // respond to a menu item click, call intent to go to Contact activity
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.menu_contact){
            Class destinationActivity = ContactActivity.class;

            // create intent to go to destination
            Intent startContactActivityIntent = new Intent(MainActivity.this, destinationActivity);
            // adding extra message to intent
            String msg = mSearchTermEditText.getText().toString(); // what user enters
            startContactActivityIntent.putExtra(Intent.EXTRA_TEXT,msg);

            startActivity(startContactActivityIntent);
            Log.d("info","Contact Activity Lanunched");

        }

        return true;
    }
} // end of main activity