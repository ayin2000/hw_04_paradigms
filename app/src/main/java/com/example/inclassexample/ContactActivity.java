package com.example.inclassexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactActivity extends AppCompatActivity {

    private TextView mDisplayAboutTextView;
    private Button mOpenWebPageButton;
    private Button mOpenMapButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        // connect with xml visual elements
        mDisplayAboutTextView = (TextView)findViewById(R.id.tv_about_text);
        mOpenWebPageButton = (Button)findViewById(R.id.button_open_webpage);
        mOpenMapButton = (Button)findViewById(R.id.button_open_map);

        // Step 1 get message from Main page  - use internet
        Intent intentThatStartedThisActivity = getIntent();
        String message = "Test";
        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            message = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mDisplayAboutTextView.append("\n\nMessage:\n\n" + message);
        }

        final String urlString = "https://www.pokemon.com/us/";
        //Step 2 open webpage button
        mOpenWebPageButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openWebPage(urlString);
                    }
                }
        );
        mOpenMapButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openMap();
                    }
                }
        );

    } // end of onCreate


    public void openWebPage(String urlString){
        Uri webpage = Uri.parse(urlString);

        Intent openWebPageIntent = new Intent(Intent.ACTION_VIEW, webpage); // diff construtor than openMap

        startActivity(openWebPageIntent);
    }


    public void openMap() {

        String addressString = "University of Notre Dame, IN";
        Uri addressUri = Uri.parse("geo:0,0").buildUpon().appendQueryParameter("q", addressString).build();
        Intent openMapIntent = new Intent(Intent.ACTION_VIEW);
        openMapIntent.setData(addressUri);

        startActivity(openMapIntent);
    }
}
