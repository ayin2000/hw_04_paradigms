package com.example.inclassexample.utilities;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class NetworkUtils {

    public static URL buildCountriesUrl(){
        String countryUrlString = "https://gist.githubusercontent.com/jherr/23ae3f96cf5ac341c98cd9aa164d2fe3/raw/f8d792f5b2cf97eaaf9f0c2119918f333e348823/pokemon.json";
        URL countryUrl = null;
        try{
            countryUrl = new URL(countryUrlString);
        } catch(MalformedURLException e){
            e.printStackTrace();
        }
        return countryUrl;
    }// return URL object



    public static String getResponseFromUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try{
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in); // connection to inputstream
            scanner.useDelimiter("\\A"); // delimiter for end of message (use end of message instead of space)
            // read url
            boolean hasInput = scanner.hasNext(); // hasnNext returns true/false
            if(hasInput){
                return scanner.next(); // gets one (next)word
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.d("Debug", "reading from url source failed");
        }
        return null;

    }

    public static ArrayList<String> typeReturn(String countriesResponseString, String inputName){
        ArrayList<String> typeList = new ArrayList<String>();
        try {
            JSONArray allCountriesArray = new JSONArray(countriesResponseString);
            Log.d("debug", "parsing types");
            for(int i = 0; i < allCountriesArray.length(); i++){
                JSONObject childJson = allCountriesArray.getJSONObject(i);
                if(childJson.has("name")) {
                    JSONObject englishJson = childJson.getJSONObject("name");
                    String name = englishJson.getString("english");
                    if (name.equals(inputName)){
                        Log.d("debug", "sucessful");
                        typeList.add(childJson.getString("type"));
                    }
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
            Log.d("debug", "JSON parsing failed");
        }
        return typeList;

    }

    public static ArrayList<String> parsePokemon(String countriesResponseString){
        ArrayList<String> countryList = new ArrayList<String>();
        try {
            JSONArray allCountriesArray = new JSONArray(countriesResponseString);
            Log.d("debug", "parsing countries");
            for(int i = 0; i < allCountriesArray.length(); i++){
                JSONObject childJson = allCountriesArray.getJSONObject(i);
                if(childJson.has("name")) {
                    JSONObject englishJson = childJson.getJSONObject("name");
                    String name = englishJson.getString("english");
                    if (name != null) countryList.add(name);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
            Log.d("debug", "JSON parsing failed");
        }
        return countryList;

    }

}
